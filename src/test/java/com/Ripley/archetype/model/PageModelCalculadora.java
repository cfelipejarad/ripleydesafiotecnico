package com.Ripley.archetype.model;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PageModelCalculadora {

	@FindBy(how = How.XPATH, using = "//div[@class='result']//span")
	private WebElement Resultado;

	public WebElement getResultado() {
		return Resultado;
	}

	public void setResultado(WebElement resultado) {
		Resultado = resultado;
	}

}
