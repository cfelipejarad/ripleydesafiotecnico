package com.Ripley.archetype.definition;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Ripley.archetype.flow.BaseFlow;
import com.Ripley.archetype.util.GenericMethods;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RipleyCalculadoraDefinition {

	 
	@Given("^me encuentro con el aplicativo calculadora$")
	public void me_encuentro_con_el_aplicativo_calculadora() throws Throwable {
		assertTrue("no se Encuentra en aplicacion calculadora", true);

	}

	@Then("^Valido que exista botones \"([^\"]*)\" calculadora$")
	public void valido_que_exista_botones_calculadora(String btn) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		assertTrue(" no existe botón " + btn,
				GenericMethods.existElement(By.xpath("//button//span[text()='" + btn + "']")));
	}

	@Then("^valido que exista pantalla de resultado calculadora$")
	public void valido_que_exista_pantalla_de_resultado_calculadora() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		assertTrue("no existe pantalla resultado ", BaseFlow.pagemodelCalculadora.getResultado().isDisplayed());
	}

	@When("^ingreso \"([^\"]*)\" en calculadora$")
	public void ingreso_en_calculadora(String btn) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		GenericMethods.clickElement(BaseFlow.driver.findElement(By.xpath("//button//span[text()='" + btn + "']")));

	}

	@When("^Selecciono funcionalidad \"([^\"]*)\"$")
	public void selecciono_funcionalidad(String btn) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		GenericMethods.clickElement(BaseFlow.driver.findElement(By.xpath("//button//span[text()='" + btn + "']")));

	}

	@Then("^valido resultado y comparo \"([^\"]*)\"$")
	public void valido_resultado_y_comparo(String Valor) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		String valorPantalla = BaseFlow.pagemodelCalculadora.getResultado().getText();

		assertTrue("Valor no coincide  muestra  " + valorPantalla + " y debe mostrar  " + Valor,
				Valor.equals(valorPantalla));

	}

	@Then("^doy click  en boton borrar$")
	public void doy_click_en_boton_borrar() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		GenericMethods.clickElement(BaseFlow.driver.findElement(By.xpath("//button//span[text()='C']")));

	}

}
