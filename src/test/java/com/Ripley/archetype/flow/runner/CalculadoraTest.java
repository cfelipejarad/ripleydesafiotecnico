package com.Ripley.archetype.flow.runner;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;
import com.Ripley.archetype.flow.BaseFlow;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/features" }, glue = "com/Ripley/archetype/definition", tags = {
		"@Calculadora" }, plugin = { "com.cucumber.listener.ExtentCucumberFormatter:Reporte/Resultados.html" })

public class CalculadoraTest extends BaseFlow {
	@AfterClass
	public static void setUpFinal() throws Exception {
		// String rutaXML = System.getProperty("user.dir") +
		// "/src/test/resources/extent-config.xml";
		// Reporter.loadXMLConfig(rutaXML);
	}
}
