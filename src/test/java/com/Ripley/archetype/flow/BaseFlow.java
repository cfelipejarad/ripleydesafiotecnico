package com.Ripley.archetype.flow;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import com.Ripley.archetype.model.PageModelCalculadora;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

@RunWith(Suite.class)
public class BaseFlow {
	public static PageModelCalculadora pageModel;
	public static AndroidDriver<MobileElement> driver;
	public static IOSDriver<IOSElement> driverIos;

	public static DesiredCapabilities cap = null;
	public static String sandbox;
	public static URL url;
	private final static String APP_PACKAGE_NAME = "io.testproject.demo";
	private final static String APP_ACTIVITY_NAME = ".MainActivity";
	private final static String PASSWORD = "12345";
	public static PageModelCalculadora pagemodelCalculadora;
	private final static String Mobile = "Android"; // IOS;

	@BeforeClass
	public static void InitializeWebDriver() throws Exception {
		setAppium();
		pagemodelCalculadora = PageFactory.initElements(driver, PageModelCalculadora.class);

	}

	public static void setAppium() throws MalformedURLException {
		final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
		url = new URL(URL_STRING);
		switch (Mobile) {

		case "Android":

			DesiredCapabilities capabilities = DesiredCapabilities.android();
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
			capabilities.setCapability(MobileCapabilityType.UDID, "Ras8N85NXQFR");
			capabilities.setCapability(MobileCapabilityType.NO_RESET, false);

			capabilities.setCapability("platformVersion", "10");
			capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, APP_PACKAGE_NAME);
			// capabilities.setCapability(AndroidMobileCapabilityType.BROWSER_NAME,
			// "CHROME");

			driver = new AndroidDriver<MobileElement>(url, capabilities);
			// Discard state
			driver.resetApp();
			break;

		case "IOS":

			capabilities = new DesiredCapabilities();
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone Simulator");
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11.4");
			capabilities.setCapability(MobileCapabilityType.APP, APP_PACKAGE_NAME);
			capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
			capabilities.setCapability("useNewWDA", false);
			// 4
			driverIos = new IOSDriver<IOSElement>(url, capabilities);
			driverIos.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			driverIos.resetApp();
			break;
		}

	}

	@AfterClass
	public static void setUpFinal() throws Exception {
		driver.quit();
	}
}
