
 ```
# Proyecto Desafio tecnico Banco Ripley

_Proyecto Automatizacion de pruebas funcionales Sobre un prototipo de una calculadora corresponde a un desafío tecnico_
1.- Se consideran las siguientes Validaciones: 
	a) Validación que exista todo los elemento (botones de numero, pantalla de resultado )
	b) Validación de operaciones Suma , resta , division, multiplicación  con su respectivo resultado
 	c) Validación de operaciones continuas (suma, resta division, y multiplicacion) con su respectivo resultado
	d) validación de operaciones combinadas con su respectivo resultado
	
2.- Caso borde que se consideran 
	a) Division por cero 
	b) Largo de numero de resultado que no sobrecarguen la variable
	c) Concatenación de la tecla . 
   
 

 


 


### Resultados de pruebas 🔩

_El Ejecutor de esta demo se encuentra en la carpeta src\test\java\com\Ripley\archetype\flow


## Construido con 🛠️

_Todas las tecnologias utilizadas se encuentran en archivo pom.xml ._ 

## Autor ✒️

* **Felipe Jara **   - *QA Automation SR* - (cfelipe.jarad@gmail.com)