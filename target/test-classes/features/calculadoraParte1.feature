#Author: Carlos Felipe Jara Diaz
#Keywords Summary :
#Feature: Validación  Parte 1 de documento desafio tecnico QA
#
@Calculadora
Feature: Validación ejemplo practico calculadora

  Scenario Outline: Valido que exista creado botón de calculadora
    Given me encuentro con el aplicativo calculadora
    Then Valido que exista botones <botones> calculadora

    Examples: 
      | botones |
      | "1"     |
      | "2"     |
      | "3"     |
      | "4"     |
      | "5"     |
      | "6"     |
      | "7"     |
      | "8"     |
      | "9"     |
      | "0"     |
      | "."     |
      | "+"     |
      | "/"     |
      | "-"     |
      | "*"     |
      | "="     |
      | "CE"    |
      | "C"     |

  Scenario: Validación que exista pantalla resultado
    Given me encuentro con el aplicativo calculadora
    Then valido que exista pantalla de resultado calculadora

  @ValidacionSumas @Funcionalidades
  Scenario Outline: Validación suma
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "+"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "="
    Then valido resultado y comparo <resultado>
    And doy click  en boton borrar

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "5"    | "3"    | "8"       |
      | "6"    | "0"    | "6"       |
      | "2"    | "1"    | "3"       |
      | "9"    | "9"    | "18"      |

  Scenario: Doy click en limpiar
    Then doy click  en boton borrar

  @ValidacionSumas @Funcionalidades
  Scenario Outline: Validación suma Continua
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "+"
    When ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "+"
    Then valido resultado y comparo <resultado>

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "5"    | "5"    | "10"      |
      | "3"    | "8"    | "21"      |
      | "9"    | "1"    | "31"      |
      | "3"    | "2"    | "36"      |

  @ValidacionResta @Funcionalidades
  Scenario Outline: Validación resta
    Given me encuentro con el aplicativo calculadora
    And doy click  en boton borrar
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "-"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "="
    Then valido resultado y comparo <resultado>
    And doy click  en boton borrar

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "5"    | "3"    | "2"       |
      | "6"    | "0"    | "6"       |
      | "2"    | "1"    | "1"       |
      | "9"    | "9"    | "18"      |

  Scenario: Doy click en limpiar
    Then doy click  en boton borrar

  @ValidacionResta @Funcionalidades
  Scenario Outline: Validación resta Continua
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "-"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "-"
    Then valido resultado y comparo <resultado>

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "9"    | "0"    | "9"       |
      | "1"    | "2"    | "6"       |
      | "2"    | "2"    | "4"       |

  Scenario: Doy click en limpiar
    Then doy click  en boton borrar

  @ValidacionMultiplicacion @Funcionalidades
  Scenario Outline: Validación ValidacionMultiplicacion
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "*"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "="
    Then valido resultado y comparo <resultado>
    And doy click  en boton borrar

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "5"    | "3"    | "15"      |
      | "6"    | "0"    | "0"       |
      | "1"    | "1"    | "1"       |
      | "9"    | "9"    | "81"      |

  Scenario: Doy click en limpiar
    Then doy click  en boton borrar

  @ValidacionSmultiplicacion @Funcionalidades
  Scenario Outline: Validación multiplicacion Continua
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "*"
    Then valido resultado y comparo <resultado>

    Examples: 
      | Valor1 | resultado |
      | "2"    | "2"       |
      | "3"    | "6"       |
      | "4"    | "24"      |

  @ValidacionDivision @Funcionalidades
  Scenario Outline: Validación ValidacionMultiplicacion
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "*"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "="
    Then valido resultado y comparo <resultado>
    And doy click  en boton borrar

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "5"    | "3"    | "15"      |
      | "6"    | "0"    | "0"       |
      | "1"    | "1"    | "1"       |
      | "9"    | "9"    | "81"      |

  Scenario: Doy click en limpiar
    Then doy click  en boton borrar

  @Validaciondivision @Funcionalidades
  Scenario Outline: Validación division Continua
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "/"
    Then valido resultado y comparo <resultado>

    Examples: 
      | Valor1 | resultado |
      | "9"    | "9"       |
      | "3"    | "3"       |
      | "2"    | "1.5"     |

  @ValidacionOperaciones @Funcionalidades
  Scenario Outline: Validación operaciones suma resta multiplicacion division
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad <operatoria>
    Then valido resultado y comparo <resultado>

    Examples: 
      | Valor1 | operatoria | resultado |
      | "2"    | "+"        | "2"       |
      | "5"    | "*"        | "7"       |
      | "2"    | "+"        | "14"      |
      | "6"    | "-"        | "20"      |
      | "2"    | "*"        | "18"      |
      | "9"    | "/"        | "162"     |
      | "3"    | "="        | "54"      |

  Scenario: Doy click en limpiar
    Then doy click  en boton borrar

  @ValidacionSumasNumerosNegativas @Funcionalidades
  Scenario Outline: Validación operaciones suma numeros Negativos
    Given me encuentro con el aplicativo calculadora
    When Selecciono funcionalidad "-"
    And ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "+"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "="
    Then valido resultado y comparo <resultado>
    And doy click  en boton borrar

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "2"    | "2"    | "0"       |
      | "5"    | "1"    | "4"       |
      | "2"    | "3"    | "-1"      |
