#Author: Carlos Felipe Jara Diaz
#Keywords Summary :
#Feature: Validación  Parte 2 de documento desafio tecnico QA Casos Borde
#
@Calculadora @CasoBorde
Feature: Validación Caso borde practico calculadora

  Scenario Outline: Validación caso borde division por .
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
    And Selecciono funcionalidad "/"
    And ingreso <Valor2> en calculadora
    And Selecciono funcionalidad "="
    Then valido resultado y comparo <resultado>
    And doy click  en boton borrar

    Examples: 
      | Valor1 | Valor2 | resultado |
      | "5"    | "0"    | "Indefinido"       |
      | "0"    | "5"    | "0"       |
      | 
 
  @ValidacionSumas @Funcionalidades 
  Scenario Outline: Ingreso largo de un entero para evitar sobrecarga de variable
    Given me encuentro con el aplicativo calculadora
    When ingreso <Valor1> en calculadora
     
#2147483647
    Examples: 
      | Valor1 |    
      | "5"    |  
      | "3"    |  
      | "9"    |  
      | "3"    |  
 | "3"    |  
      | "3"    |  
      | "3"    |  
      | "3"    |  
      | "3"    |  
      | "3"    |  
      